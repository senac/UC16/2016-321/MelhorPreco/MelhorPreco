/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.melhorpreco.entity.Cliente;

/**
 *
 * @author wilso
 */
public class ClienteDAO extends DAO<Cliente>{
    
    public ClienteDAO(){
        super(Cliente.class);
    }
    
}
